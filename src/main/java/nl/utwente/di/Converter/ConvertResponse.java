package nl.utwente.di.Converter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

public class ConvertResponse extends HttpServlet {
    // FIELD VARIABLES: ------------------------------------------------------
    /**
     * Identifier to serialize/deserialize an object.
     */
    private static final long serialVersionUID = 1L;
    private Converter converter;

    /**
     * This method is used to instantiate the converter and ensure that the Servlet is initialized.
     * @throws ServletException
     */
    public void init() throws ServletException {
        converter = new Converter();
    }

    /**
     * Corresponds to HTML GET request to the URI, and is the procedure followed to respond to such requests.
     * In this case, it presents a simple HTML page with fields requiring user input.
     * @param request of type HttpServletRequest, representing the body of the request.
     * @param response of type HttpServletResponse, representing the body of the response.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        converter = new Converter();
        // Declaring response type as HTML web-page.
        response.setContentType("text/html");
        // Using the PrintWriter of the response object.
        PrintWriter out = response.getWriter();
        // Setting the HTML title as a variable:
        String title = "Celsius to Fahrenheit Converter";
        String[] converted = converter.getFahrenheit(request.getParameter("CF"));
        if (converted.length == 2) {
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + "Error, you typed: " + converted[1] + ", but this is not a number, please press back and try again." + "</H1>\n" + "</BODY></HTML>");
        } else {
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Celsius: " +
                    request.getParameter("CF") + "\n" +
                    "  <P>Fahrenheit: " +
                    converter.getFahrenheit(request.getParameter("CF"))[0] +
                    "</BODY></HTML>");
        }
    }
}
