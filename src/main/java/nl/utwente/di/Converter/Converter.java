package nl.utwente.di.Converter;

public class Converter {
    /**
     * This method receives a parameter of the temperature in Celsius.
     * @param temperatureCelsius of type double, representing the temperature in Celsius.
     * @return of type String[], representing the Fahrenheit conversion of which the size is used to deduce if invalid input occurred.
     */
    public String[] getFahrenheit(String temperatureCelsius) {
        String fahr = "INVALID";
        String[] result;
        try {
            double fahrNum = Double.parseDouble(temperatureCelsius);
            fahr = (fahrNum * 9/5) + 32 + "";
            result = new String[]{fahr};
        } catch (NumberFormatException e) {
            result = new String[]{fahr, temperatureCelsius};
        }
        return result;
    }
}
